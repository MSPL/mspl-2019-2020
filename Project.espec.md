# Mini-Projet

The Mini-Projet has to be developed in *groups of two students*.

## Deadlines

- The report (see below for the instructions) needs to be in your git repository before the 19/03/2020 at 23:59.
- The project presentation will be on 24/03/2020 morning.
- You are allowed to apply updates to your report until the 24/03/2020 at 08:00.

## Important

You have to write a basic README file as soon as possible, following the [provided template](Projet/README_model.md).
You need to rename this file as `README_name1_name2.md`.

## Datasets

Non exhaustive list where data sets may be chosen from:

- FR Data.gouv: https://www.data.gouv.fr/fr/
- Insee: https://www.insee.fr/fr/accueil
- Kaggle: https://www.kaggle.com/datasets
- US Data.gov: https://catalog.data.gov/dataset
- Our World in Data: https://ourworldindata.org/

## Guidelines

1. Justify the data set selection and described it
2. Formulate three questions about the selected data set
3. Discuss with the professors and choose one question among these
4. Propose a methodology to answer that question
5. Implement this methodology using Literate Programming
6. The report must be acessible online (in your git repository)

## Report

The report has to be written with Rstudio or Jupyter, using R or Python. You need to provide both the original file
(`*.Rmd` or `*.ipynb`) and a compiled file (`*.pdf` or `*.html`). Both files need to be placed in the `Projet` directory of
your git repository. The files must be named `projet_MSPL_name1_name2.extension` (for instance,
`projet_MSPL_Vincent_Cornebize.Rmd`).

The report must contain:
- Frontpage
  - _Title_
  - Student's name
- Table of contents
- Introduction
  - Context / Dataset description
    - How the dataset has been obtained?
  - Description of the question
- Methodology
  - Data clean-up procedures
  - Scientific workflow
  - Data representation choices
- Analysis in Literate Programming
- Conclusion
- References

## Important notes

The question might be from simple to complex. More the question is
simple, more deep should be the analysis.

## Groups

Your project defence will be composed of (strictly) 5 minutes of presentation
and 2 minutes of questions. The presentation support could be anything: if you
have a PDF, you have to provide it to us in advance, if you need your own
machine be prepared (setup time is part of your defence time).

| Nom 1                   | Nom 2                 | Theme                                                | Site                                      | URL                                                                     | Rendu |
| ----------------------- | --------------------- | ---------------------------------------------------- | ----------------------------------------- | ----------------------------------------------------------------------- | ----- |
| Yanis Haddad            | Marc Sabatier         | Foot                                                 | kaggle                                    | <https://gricad-gitlab.univ-grenoble-alpes.fr/sabamarc/mspl-2019-2020>  |   ✓   |
| Aurélien Vallet         | Khadim Mbengue        | Sécurité routière                                    | data.gouv.fr                              | <https://gricad-gitlab.univ-grenoble-alpes.fr/valleta/mspl-2019-2020>   |   ✓   |
| Frédéric Prochnow       | Samuel Monsempes      | Chomage                                              | Insee                                     | <https://gricad-gitlab.univ-grenoble-alpes.fr/prochnof/mspl-2019-2020>  |   ✓   |
| Ronny Suriano           | Benjamin Pellissier   | Basket NBA                                           | Kaggle                                    | <https://gricad-gitlab.univ-grenoble-alpes.fr/pellissb/mspl-2019-2020>  |   ✓   |
| Abdou Karim Ndiaye      | Khadidiadou Diagne    | Qualité de l'air                                     | ?                                         | <https://gricad-gitlab.univ-grenoble-alpes.fr/ndiaabdo/mspl-2019-2020>  |   ✓   |
| Thomas Laurent          | Quentin Matraire      | Criminalité en France                                | data.gouv.fr                              | <https://gricad-gitlab.univ-grenoble-alpes.fr/matrairq/mspl-2019-2020>  |   ✓   |
| Valentin Duprey         | Loris Guerrin         | Prénoms / série aux US                               | Kaggle                                    | <https://gricad-gitlab.univ-grenoble-alpes.fr/guerrilo/mspl-2019-2020>  |   ✓   |
| Walid Boukris           | Said Berrima          | AirBnB à New-York                                    | Kaggle                                    | <https://gricad-gitlab.univ-grenoble-alpes.fr/boukrisw/mspl-2019-2020>  |   ✓   |
| Florian Caillol         | Diallo Ahma           | Pratique sportive et météo                           | datagouv.fr                               | <https://gricad-gitlab.univ-grenoble-alpes.fr/caillolf/mspl-2019-2020>  |   ✓   |
| Pierre De Lemos Almeida | Clement Dumas1        | Taille dans population mondiale                      | ourworldindata donnees.banquemondiale.org | <https://gricad-gitlab.univ-grenoble-alpes.fr/dumaclem/mspl-2019-2020>  |   ✓   |
| Mohamed Diallo          | Farid Meziane         | Espérance de vie                                     | ined                                      | <https://gricad-gitlab.univ-grenoble-alpes.fr/diallomo/mspl-2019-2020>  |   ✓   |
| Julien Dufourt          | Robin Chappuis        | Accidents de ciculation en France                    | data.gouv.fr                              | <https://gricad-gitlab.univ-grenoble-alpes.fr/chappuir/mspl-2019-2020>  |   ✓   |
| Bruno Disson            | Zakari Abdoumahamadou | Trafic métro                                         | data ratp                                 | <https://gricad-gitlab.univ-grenoble-alpes.fr/dissonb/mspl-2019-2020>   |   ✓   |
| Vafing Bakayoko         | Allassane John Fomba  | Étudiants du supérieur                               | data.gouv                                 | <https://gricad-gitlab.univ-grenoble-alpes.fr/fombaa/mspl-2019-2020>    |   ✓   |
| Adiiya Gilmanova        | Hawa Sogoba           | Empreinte carbone et espérance de vie en bonne santé | ue 27                                     | <https://gricad-gitlab.univ-grenoble-alpes.fr/gilmanoa/mspl-2019-2020>  |   ✓   |
| Ibrahim Coulibaly       | Sacha Bonneville      | Taux d'emplois                                       | insee                                     | <https://gricad-gitlab.univ-grenoble-alpes.fr/bonnesac/mspl-2019-2020>  |   ✓   |
| Mohamed Galmami         | Kahlaoui Ghaieth      | Les maladies cardiovasculaires                       | www.ecosante.fr                           | <https://gricad-gitlab.univ-grenoble-alpes.fr/kahlaoug/mspl-2019-2020>  |   ✓   |
| Ousmane Sow             | Mohamed Ait Laadik    | Crimes au Texas                                      | ?                                         | <https://gricad-gitlab.univ-grenoble-alpes.fr/sowou/mspl-2019-2020>     |   ✓   |
| Roméo Agossou           | Mariama Diallo        | SNCF en France                                       | Insee & data.gouv.fr                      | <https://gricad-gitlab.univ-grenoble-alpes.fr/agossoud/mspl-2019-2020>  |   ✓   |
| Yang Zhuangzhuang       | Yuan Rong             | Emissions de polluants des véhicules en France       | data.gouv.fr                              | <https://gricad-gitlab.univ-grenoble-alpes.fr/yangzhua/mspl-2019-2020>  |   ✓   |
| Mathis Vigne            | Fatima-Zahra Marirh   | Les français expatriés                               | data.gouv.fr                              | <https://gricad-gitlab.univ-grenoble-alpes.fr/vignema/mspl-2019-2020>   |   ✓   |
| Imane Lyass             | Rayhana El Aouadi     | Progrès médical et mortalité infantile               | Insee                                     | <https://gricad-gitlab.univ-grenoble-alpes.fr/lyassi/mspl-2019-2020>    |   ✓   |
| Romain Revil            | Quentin Pinet         | Évolution de l'IDH dans le monde                     | banquemondiale.org                        | <https://gricad-gitlab.univ-grenoble-alpes.fr/pinetq/mspl-2019-2020>    |   ✓   |
| Mamadou Sow             | Younes Kebbabi        | Emploi en France                                     | Insee                                     | <https://gricad-gitlab.univ-grenoble-alpes.fr/sowma/mspl-2019-2020>     |   ✓   |
| Idriss Sagara           | Sylvain Tuyishimire   | Changement climatique                                | Kaggle                                    | <https://gricad-gitlab.univ-grenoble-alpes.fr/sagarai/mspl-2019-2020>   |   ✓   |

Group of 3 students (exceptional authorization):

| Nom 1                   | Nom 2                 | Nom 3                 | Theme                                                | Site                                      | URL                                                                    | Rendu |
| ----------------------- | --------------------- | --------------------- | ---------------------------------------------------- | ----------------------------------------- | ---------------------------------------------------------------------- | ----- |
| Ismail Badzi            | Mohammed Elasri       | Mohammed Elbaroudi    | Notes sur le Google play store                       | ?                                         | <https://gricad-gitlab.univ-grenoble-alpes.fr/elasrim/mspl-2019-2020>  |   ✓   |
